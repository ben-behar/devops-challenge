#!/bin/bash 

echo "beginning to wait for service to come steady (will wait 10 minutes)"
let retries=60
while let retries-- && !(aws ecs --region=us-east-2 describe-services --cluster example_cluster --services nginx-example | jq .services[0].events[0].message | grep steady)
do
  sleep 10
  echo "."
done

if (aws ecs --region=us-east-2 describe-services --cluster example_cluster --services nginx-example | jq .services[0].events[0].message | grep steady)
then
  echo "service seems to be successfully deployed!"
else
  echo "didn't seem to work after 10 minutes"
  exit 2
fi
cd ./aws_infrastructure/alb/
dns_name=$(terraform output -raw dns_name)
if (curl $dns_name | grep "container" )
then 
  echo "it is responding as expected, everything seems to be working."
  echo "see for yourself: http://${dns_name}"
  echo "SUCCESS"
  exit 0
else
  echo "not getting the expected response"
  curl $dns_name
  echo "FAILURE"
  exit 2
fi
