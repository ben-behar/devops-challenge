#!/bin/bash
set -ex 

echo 'we destroy the ecs resources'
cd aws_infrastructure/ecs/
terraform init
terraform destroy -auto-approve

echo 'now we destroy the alb'
cd ../alb/
terraform init
terraform destroy -auto-approve

echo "now we destroy the ecr"
cd ../ecr/
terraform init
terraform destroy -auto-approve

