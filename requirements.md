# Lendflow DevOps Assessment

# Goal

Create a script or IaC-powered method to deploy a containerized webserver to AWS, as well as a script to check that the service is running and publicly available. 

# Prerequisites

You will need an AWS account. Create one if you don't own one already. You can use free-tier resources for this test. You will also need a free personal BitBucket account.

# The Task

You are required to set up a new Application Load-Balancer, ECS Cluster, and ECS Service in AWS. It must:

* Be a publicly accessible web service.
* Run Nginx.
* Display textual content. This can be as simple as some static text representing a version number like `4.3.2` or as complex as a full website. You choose. We will not provide the content. 


# Mandatory Work

* Fork this repository into your personal BitBucket account.
* Commit your code/scripts/etc in this fork.
* Alter the README to provide user instructions required to:
  * Deploy the AWS infrastructure for the webserver, in an existing VPC.
  * Run the checker script.
* Provide us IAM credentials to log in to the AWS account, via email. If you have other resources in it make sure we can only access what is related to this test.
* Automate as much as possible.
* Document each step. 
* Make it easy to deploy.

Give us access to your fork, and send us an email when you’re done. Feel free to ask questions if anything is unclear, confusing, or just plain missing.


# Extra Credit

We know time is precious, we won't mark you down for not doing the extra credits, but if you want to give them a go...

* Complete the challenge entirely in Terraform.
* Add Cloudwatch alarms for relevant performance metrics.



# Questions

#### **What scripting languages can I use?**

Anyone you like. You’ll have to justify your decision. Please pick something you're familiar with, as you'll need to be able to discuss it.

#### **Will I have to pay for the AWS charges?**

No. You are expected to use free-tier resources only and not generate any charges. Please remember to delete your resources once the review process is over so you are not charged by AWS.

#### **What will you be grading me on?**

Scripting skills, elegance, maintainability, understanding of the technologies you use, security, and in case you missed it....documentation!

We don’t want to know if you can do exactly as asked (or everybody would have the same result). We want to know what you bring to the table when working on a project, and how you innovate to make processes better. More features? Best solution? Thinking outside the box?

Hint: we would like to be able to test this outside of the environment you create, so make it reusable

#### **Will I have a chance to explain my choices?**

Feel free to comment your code, or ideally put explanations in a pull request within the repo in one or more README files
We’ll discuss the choices you made on the next interview, if applicable.

#### **Why doesn't the test include X?**

Good question. Feel free to tell us how to make the test better. Or, you know, fork it and improve it!

#### **How long should this take?**

There are many ways to solve this problem so it may vary for each candidate and depends on how far you want to take it but we are confident the basic requirements can be met with 2-3 hours of work.
