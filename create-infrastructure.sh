#!/bin/bash
set -ex 
export region=us-east-2
export ecr_host=950711726411.dkr.ecr.us-east-2.amazonaws.com

echo "first we create the ECR repository so we can download our container later when we start our ecs task"
cd ./aws_infrastructure/ecr/
terraform init
terraform apply -auto-approve

echo 'now that the ECR is setup we want to build/upload our container'
cd ../../nginx_container
aws ecr get-login-password --region $region | docker login --username AWS --password-stdin ${ecr_host}
docker build -t ${ecr_host}/nginx_example .
docker push ${ecr_host}/nginx_example

echo 'now we setup the alb'
cd ../aws_infrastructure/alb/
terraform init
terraform apply -auto-approve

echo 'now we create the ecs resources'
cd ../ecs
terraform init
terraform apply -auto-approve

echo "Finished all terraform applies!"
