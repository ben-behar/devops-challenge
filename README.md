# Simple Nginx AWS ECS Example

Purpose: to demonstrate an example of how one would use terraform and AWS to create a publicly available web application using ECS.

## Setup

I created the folder .devcontainer to make development straight forward.  It has a Dockerfile within it that you can build to obtain all of the versions of the binaries that you might need to apply the infrastructure and deploy the web application.  It is configured for use with a plugin to VSCode that you may not have installed. If you do, just select `reopen in container`, and if not here are some commands that should construct the container for you and obtain a working shell inside of the container:

    cd .devcontainer
    docker build -t devcontainer .
    cd ../
    docker run -it --workdir=/workspaces/devops-challenge -v $(pwd):/workspaces/devops-challenge -v /var/run/docker.sock:/var/run/docker.sock devcontainer

Now you should be running a shell inside of the container.  Then you should setup your aws credentials that were emailed to you, by using aws configure and just following the prompts (region = us-east-2).

    aws configure

Then just to validate that that's working try out:

    aws iam get-user

At this point you should be good to run the terraform directly.

# Usage

I wrote a script that should automatically set everything up live. However it skips creating the VPC since the directions specified "an existing VPC" should be used.  If you want to use the VPC I created everything should be setup already, but if not you'll need to change the vpc_id and subnet_ids found in the alb and ecs vars.tf to be what you actually want.  This script should work to use the currently existing VPC:

    ./create-infrastructure.sh

Otherwise you can use a different vpc that you created by specifying it.  Though there may be other variables and vpc configurations that I haven't fully tested in the terraform.

It will break on the first error so if that happens feel free to let me know and I can take a look, otherwise, you can try stepping through it manually.  The order of applies should look something like this:
 
 - apply ./aws_infrastructure/ecr
 - (apply ./aws_infrastructure/vpc) #optionally
 - apply ./aws_infrastructure/alb
 - apply ./aws_infrastructure/ecs

To validate that the whole stack is working, the script validate-infrastructure.sh can be used:

   ./validate-infrastructure.sh

To destroy all resources use the script
   
   ./destroy-infrastructure.sh


# Explanation

To summarize the steps that are being taken:
 
 - the ecr terraform will create an ecr repository for us to upload our web-app ("nginx_container")
 - the alb terraform will create an ALB with listener in the specified VPC/subnets, along with a security group to limit public traffic to approved/trusted public IP addresses
 - the ecs terraform will create the ecs cluster/service/task combination that allows the web-app to be deployed, along with a target group and listener to direct traffic toward it from the alb, and an additional security group to make sure that traffic is not skipping the alb.
 - the vpc terraform is not a part of the exercise but it was something that I needed some practice with so I included it to make starting over from scratch easy, if this needed to be moved to another aws account for example.
 - the iam terraform simply creates a user that should be used to apply terraform and inspect configurations, it is the source of the user credentials I am sending via email.

