output "private_subnet_ids" {
  value = [ for key, value in aws_subnet.private : value.id ]
}

output "public_subnet_ids" {
  value = [ for key, value in aws_subnet.public : value.id ]
}

output "vpc_id" {
  value = aws_vpc.example_vpc.id
}