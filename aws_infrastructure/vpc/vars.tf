variable "aws_region" {
  description = "the region in which to create everything"
  type        = string
  default     = "us-east-2"
}
