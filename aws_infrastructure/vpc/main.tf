# VPC 
## Purpose: creates a single vpc with public and private subnets and an internet gateway

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

# Create a VPC
resource "aws_vpc" "example_vpc" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
  cidr_block = "10.0.0.0/16"
}


# Create an internet gateway
resource "aws_internet_gateway" "internet_gateway" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway
  vpc_id = aws_vpc.example_vpc.id
}

resource "aws_subnet" "public" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet#map_public_ip_on_launch
  for_each = {
    a = "10.0.1.0/24"
    b = "10.0.2.0/24"
  }
  availability_zone = "${var.aws_region}${each.key}"
  vpc_id     = aws_vpc.example_vpc.id
  cidr_block = each.value
  map_public_ip_on_launch = false # this would normally be true but if it's not needed yet I'll continue to leave it off
  tags = {
    Name = "Public-${each.key}"
  }
}

resource "aws_route_table_association" "public" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
  for_each = aws_subnet.public
  subnet_id      = each.value.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table" "public" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name = "public"
  }
}

resource "aws_subnet" "private" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet#map_public_ip_on_launch
  for_each = {
    a = "10.0.11.0/24"
    b = "10.0.12.0/24"
  }
  vpc_id     = aws_vpc.example_vpc.id
  availability_zone = "${var.aws_region}${each.key}"
  cidr_block = each.value
  map_public_ip_on_launch = false
  tags = {
    Name = "Private-${each.key}"
  }
}

resource "aws_route_table_association" "private" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
  for_each       = aws_subnet.private
  subnet_id      = each.value.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table" "private" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name = "private"
  }
}

# TODO: Instead of hardcoding the a, b, c values in the availability zones, we can use use data aws availability zones as source of truth:
#  data "aws_availability_zones" "available" {
#    state = "available"
#  }
