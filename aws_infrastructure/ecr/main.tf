terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

resource "aws_ecr_repository" "nginx_example" {
  name                 = "nginx_example"
  image_tag_mutability = "MUTABLE"
}
