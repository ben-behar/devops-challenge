terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}

resource "aws_iam_user" "terraform_user" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user
  name = "terraform"

  tags = {}
}

resource "aws_iam_policy_attachment" "terraform_user" {
  name       = "terraform_user_policy_attachment"
  users      = [aws_iam_user.terraform_user.name]
  policy_arn = aws_iam_policy.terraform_user.arn
}

resource "aws_iam_policy" "terraform_user" {
  name        = "test-policy"
  description = "Policy to allow access to terraform"
policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "iam:Create*",
          "iam:Get*",
          "iam:Delete*",
          "iam:Put*",
          "iam:PassRole"
        ],
        Effect =  "Allow",
        Resource = [
          "arn:aws:iam::950711726411:role/nginx-execution-role"
        ]
      },{
        Action = [
          "ecr:*",
          "ec2:*",
          "ecs:*",
          "elasticloadbalancing:*",
          "iam:List*"
        ]
        Effect   = "Allow"
        Resource = "*"
      },{
        Action = [
          "iam:GetUser"
        ]
        Effect = "Allow"
        Resource = "arn:aws:iam::950711726411:user/terraform"
      }
    ]
  })
}
