variable "aws_region" {
  default = "us-east-2"
  type    = string
}

variable "additional_trusted_cidr_blocks" {
  description = "A string list of the cidr blocks that we wish to allow to access the ALB"
  type        = list(string)
  default     = ["0.0.0.0/0"] # Since I don't know what IP will be used when testing this... 
}

variable "vpc_id" {
  description = "The id of the vpc in which we want to place our ALB (please make sure this matches the vpc of the subnets)" 
  type        = string
  default     = "vpc-0fd594865a788fcc5"
}

variable "subnet_ids" {
  description = "A list of subnet ids in which we want to place our ALB (need at least 2 that are publicly routed)"
  type        = list(string)
  default     = ["subnet-07150018a19c05815", "subnet-0821dcc080b48ec46"]
}
