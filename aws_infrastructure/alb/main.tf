terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

data "aws_vpc" "vpc" {
  id = var.vpc_id
}

locals {
  trusted_cidr_blocks = concat( [data.aws_vpc.vpc.cidr_block], var.additional_trusted_cidr_blocks )
}

resource "aws_security_group" "fixed_ips" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
  name        = "allow_http_from_fixed_ips"
  description = "http from trusted IPs"
  vpc_id      = var.vpc_id

  ingress {
    description      = "Http Traffic From Fixed IPs"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = local.trusted_cidr_blocks
    ipv6_cidr_blocks = []
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_lb" "example_alb" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb
  name               = "example-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.fixed_ips.id]
  subnets            = var.subnet_ids
  enable_deletion_protection = false
}

resource "aws_lb_listener" "example_listener" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener
  load_balancer_arn = aws_lb.example_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "I'm the alb!"
      status_code  = "404"
    }
  }
}
