output "alb_arn" {
  value = aws_lb.example_alb.arn
}

output "listener_arn" {
  value = aws_lb_listener.example_listener.arn
}

output "dns_name" {
  value = aws_lb.example_alb.dns_name
}
