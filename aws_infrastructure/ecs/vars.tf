variable "aws_region" {
  description = "the region in which to create everything"
  type        = string
  default     = "us-east-2"
}

variable "vpc_id" {
  description = "The id of the vpc in which we want to place our ALB (please make sure this matches the vpc of the subnets)" 
  type        = string
  default     = "vpc-0fd594865a788fcc5"
}

variable "subnet_ids" {
  description = "A list of subnet ids in which we want to place our ECS containers (need at least 2 that are publicly routed)"
  type        = list(string)
  default     = ["subnet-060efe05ec20805f8", "subnet-06bd77db8cf699434"]
}

variable "container_image" {
  description = "The full URI of the container image"
  type        = string
  default     = "950711726411.dkr.ecr.us-east-2.amazonaws.com/nginx_example:latest"
}

variable "additional_trusted_cidr_blocks" {
  type    = list(string)
  default = [] # empty since we only access the container through the alb
}