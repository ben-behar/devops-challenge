terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

data "aws_vpc" "vpc" {
  id = var.vpc_id
}

data "terraform_remote_state" "alb" {
  backend = "local"
  config = {
    path = "../alb/terraform.tfstate"
  }
}

locals {
  trusted_cidr_blocks = concat( [data.aws_vpc.vpc.cidr_block], var.additional_trusted_cidr_blocks )
}

resource "aws_ecs_cluster" "example" {
  name = "example_cluster"  
}

resource "aws_ecs_service" "example_nginx" {
  name            = "nginx-example"
  launch_type     = "FARGATE"
  cluster         = aws_ecs_cluster.example.id
  task_definition = aws_ecs_task_definition.nginx_task.arn
  desired_count   = 1

  load_balancer {
    target_group_arn = aws_lb_target_group.nginx_example.arn
    container_name   = "nginx"
    container_port   = 80
  }
  
  network_configuration {
    subnets          = var.subnet_ids
    security_groups  = [ aws_security_group.nginx_example_sg.id ]
    assign_public_ip = true
  }
}

resource "aws_ecs_task_definition" "nginx_task" {
  family = "nginx-example"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.execution_role.arn
  container_definitions = jsonencode([
    {
      name      = "nginx"
      image     = var.container_image
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
    }
  ])
}

resource "aws_lb_target_group" "nginx_example" {
  name        = "nginx-example"
  target_type = "ip"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
}

resource "aws_lb_listener_rule" "nginx_example_listener" {
  listener_arn = data.terraform_remote_state.alb.outputs.listener_arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nginx_example.arn
  }


  condition {
    path_pattern {
      values = ["/*"]
    }
  }

  # condition {
  #   host_header {
  #     values = ["test.behar.cloud"]
  #   }
  # }
}

resource "aws_iam_role" "execution_role" {
  name = "nginx-execution-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "execution_role_policy" {
  name  = "nginx_execution_role_policy"
  role  = aws_iam_role.execution_role.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ecr:GetAuthorizationToken",
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_security_group" "nginx_example_sg" { # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
  name        = "nginx_example_sg"
  description = "All traffic"
  vpc_id      = var.vpc_id

  ingress {
    description      = "All TCP Traffic From Fixed IPs"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = local.trusted_cidr_blocks
    ipv6_cidr_blocks = []
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
